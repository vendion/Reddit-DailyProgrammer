package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	input := "Eye of newt"
	//input := "rugged Russian bear"
	file, err := ioutil.ReadFile("macbeth.txt")
	if err != nil {
		panic(err)
	}

	text := string(file)
	lines := strings.Split(text, "\n")

	for num, line := range lines {
		if strings.Contains(line, input) {
			num -= 1
			var empty bool
			for empty {
				if lines[num] == "" {
					empty = true
				} else {
					num -= 1
				}
			}

			for {
				if lines[num] == "" {
					break
				}

				if strings.HasPrefix(lines[num], "    ") {
					fmt.Println(lines[num])
				} else {
					break
				}
				num += 1
			}
		}
	}
}
