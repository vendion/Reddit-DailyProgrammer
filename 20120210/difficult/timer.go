package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"
)

func main() {
	run := make(chan bool, 1)
	var sT time.Time
	var eT time.Time

	f, err := os.OpenFile("stopwatch.log", os.O_CREATE|os.O_APPEND|os.O_RDWR, 0644)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	fmt.Println("Console stopwatch")
	for {
		i := getInput()

		switch i {
		case "start":
			run <- true
			sT = time.Now()
			go runTimer(run, f)
		case "stop":
			run <- false
			eT = time.Now()
			timeDiff(sT, eT, f)
		case "lap":
			eT = time.Now()
			timeDiff(sT, eT, f)
			sT = eT
		case "quit":
			timeDiff(sT, eT, f)
			os.Exit(0)
		default:
			fmt.Println("Unknown command:", i)
		}
	}
}

func getInput() string {
	fmt.Print("Command [start|lap|stop|quit]: ")
	s := bufio.NewScanner(os.Stdin)
	s.Scan()

	return strings.ToLower(s.Text())
}

func runTimer(run chan bool, f *os.File) {
	c := time.Tick(1 * time.Millisecond)
	r := <-run
	for now := range c {
		f.WriteString(now.String() + "\n")
		if r == false {
			break
		}
	}
}

func timeDiff(sT, eT time.Time, f *os.File) {
	diff := eT.Sub(sT)

	f.WriteString(fmt.Sprintf("=== Time elapsed: %v ===\n", diff.String()))
	fmt.Printf("Time elapsed: %v\n", diff.String())
}
