# Challenge #2

Rating: Difficult

Challenge Link: https://www.reddit.com/r/dailyprogrammer/comments/pjsdx/difficult_challenge_2/

## Challenge

Your mission is to create a stopwatch program. this program should have start,
stop, and lap options, and it should write out to a file to be viewed later.
