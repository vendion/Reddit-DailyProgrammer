package main

import (
	"flag"
	"fmt"
	"math"
	"os"
	"strconv"
)

func main() {
	flag.Parse()
	if flag.NArg() == 0 {
		fmt.Println("usage: go run calc.go (number)")
		os.Exit(1)
	}

	args := flag.Args()
	for _, n := range args {
		num, err := strconv.Atoi(n)
		if err != nil {
			panic(err)
		}

		a := math.Pow(float64(2), float64(num))
		fmt.Printf("%d to the power of 2 is %f\n", num, a)
	}
	os.Exit(0)
}
