package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Event struct {
	ID   int8
	Name string
	Time int8
}

func main() {
	var e []Event
	printMenu()
	fmt.Print("command> ")
	s := bufio.NewScanner(os.Stdin)
	for s.Scan() {
		cmd := s.Text()

		switch strings.ToLower(cmd) {
		case "add":
			e = addEvent(e)
		case "edit":
			fmt.Print("Event ID> ")
			if s.Scan() {
				sn := s.Text()
				n, err := strconv.Atoi(sn)
				if err != nil {
					panic(err)
				}
				e = editEvent(e, int8(n))
			}
		case "list":
			listEvents(e)
		case "remove":
			fmt.Print("Event ID> ")
			if s.Scan() {
				sn := s.Text()
				n, err := strconv.Atoi(sn)
				if err != nil {
					panic(err)
				}
				e = removeEvent(e, int8(n))
			}
		case "help":
			printMenu()
		default:
			fmt.Println("Error: unknown command given")
		}
		fmt.Print("command> ")
	}
}

func printMenu() {
	fmt.Println("--- Event Organizer System ---\n")
	fmt.Println("Add:\tAdd a new event in the system")
	fmt.Println("Edit:\tEdit an existing event in the system")
	fmt.Println("List:\tLists all events in the system")
	fmt.Println("Remove:\tRemove a event from the system")
	fmt.Println("Help:\tDisplay this help text")
}

func addEvent(e []Event) []Event {
	var n Event
	s := bufio.NewScanner(os.Stdin)
	fmt.Print("Event name: ")
	s.Scan()
	n.Name = s.Text()

	count := len(e)
	n.ID = int8(count)
	fmt.Print("Events start time: ")
	s.Scan()
	t, err := strconv.Atoi(s.Text())
	if err != nil {
		panic(err)
	}
	n.Time = int8(t)

	e = append(e, n)

	return e
}

func listEvents(e []Event) {
	for _, event := range e {
		fmt.Printf("[ID: %d] Event %s starts at %d\n", event.ID, event.Name, event.Time)
	}
}

func editEvent(e []Event, n int8) []Event {
	s := bufio.NewScanner(os.Stdin)
	fmt.Print("Updated event name> ")
	if s.Scan() {
		e[n].Name = s.Text()
	}

	fmt.Print("Updated event time> ")
	if s.Scan() {
		sn := s.Text()
		nn, err := strconv.Atoi(sn)
		if err != nil {
			panic(err)
		}
		e[n].Time = int8(nn)
	}

	return e
}

func removeEvent(e []Event, n int8) []Event {
	e = append(e[:n], e[n+1])

	return e
}
