package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	s := bufio.NewScanner(os.Stdin)
	fmt.Print("Please tell me your name: ")
	var name, age, username string
	if s.Scan() {
		name = s.Text()
	}
	if err := s.Err(); err != nil {
		panic(err)
	}

	fmt.Print("Plesae tell me your age: ")
	if s.Scan() {
		age = s.Text()
	}
	if err := s.Err(); err != nil {
		panic(err)
	}

	fmt.Print("Please tell me your username: ")
	if s.Scan() {
		username = s.Text()
	}
	if err := s.Err(); err != nil {
		panic(err)
	}

	fmt.Printf("your name is %s, you are %s years old, and your username is %s\n", name, age, username)

	d := []byte(fmt.Sprintf("%s,%s,%s\n", name, age, username))
	err := ioutil.WriteFile("hello.txt", d, 0644)
	if err != nil {
		panic(err)
	}
}
