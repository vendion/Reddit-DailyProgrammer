package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"
)

func main() {
	fmt.Println("Pick a number between 1 and 100")
	time.Sleep(time.Second * 5)
	var correct bool
	guess := guessNum()
	for !correct {
		awn := displayResults(guess)
		if strings.ToLower(awn) == "correct" ||
			strings.ToLower(awn) == "quit" {
			correct = true
		} else if strings.ToLower(awn) == "higher" {
			guess += 1
		} else if strings.ToLower(awn) == "lower" {
			guess -= 1
		}
	}
	os.Exit(0)
}

func guessNum() int {
	n := rand.Intn(99)
	return n + 1
}

func displayResults(n int) string {
	fmt.Printf("Is your number %d?\n", n)
	fmt.Print("[correct|higher|lower|quit]: ")
	s := bufio.NewScanner(os.Stdin)
	s.Scan()
	return s.Text()
}
