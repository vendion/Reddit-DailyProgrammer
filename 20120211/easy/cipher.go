package main

import (
	"errors"
	"flag"
	"fmt"
	"os"
)

var offset = flag.Int("offset", 3, "the offset to use in the ceaser cipher")

const alphabet = "abcdefghijklmnopqrstuvwxyz"

func main() {
	flag.Parse()

	if flag.NArg() == 0 {
		fmt.Fprintf(os.Stderr, "error not enough arguments\nusage: %s [encrypt|decrypt] \"message\"\n", os.Args[0])
		os.Exit(1)
	}

	cmd := flag.Arg(0)
	switch cmd {
	case "encrypt":
		s, err := cipherText(flag.Arg(1))
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error:%v\n", err)
			os.Exit(1)
		}

		fmt.Printf("Original: %s\n", flag.Arg(1))
		fmt.Printf("Ciphertext: %s\n", s)
	case "decrypt":
		*offset = -*offset
		s, err := cipherText(flag.Arg(1))
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error: %v\n", err)
			os.Exit(1)
		}

		fmt.Printf("Ciphertext: %s\n", flag.Arg(1))
		fmt.Printf("Original: %s\n", s)
	default:
		fmt.Fprintf(os.Stderr, "unknown command: %s\n", cmd)
		os.Exit(1)
	}
}

func cipherText(s string) (string, error) {
	if len(s) <= 0 {
		return "", errors.New("nothing to cipher text with")
	}

	*offset %= 26
	b := []byte(s)
	for i, v := range b {
		v |= 0x20
		if 'a' <= v && v <= 'z' {
			b[i] = alphabet[(int(('z'-'a'+1)+(v-'a'))+*offset)%26]
		}
	}

	return string(b), nil
}
