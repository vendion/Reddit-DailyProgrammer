# Challenge #3

Rating: Difficult

Challenge Link: https://www.reddit.com/r/dailyprogrammer/comments/pkwgf/2112012_challenge_3_difficult/

## Challenge

Welcome to cipher day!
For this challenge, you need to write a program that will take the scrambled
words from this post, and compare them against THIS WORD LIST[1] to unscramble
them. For bonus points, sort the words by length when you are finished. Post your
programs and/or subroutines!

Here are your words to de-scramble:
mkeart
sleewa
edcudls
iragoge
usrlsle
nalraoci
nsdeuto
amrhat
inknsy
iferkna
